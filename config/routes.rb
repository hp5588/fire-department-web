Rails.application.routes.draw do
  # get 'log/find'
  # get 'log/list'
  get 'updates/check'
  get 'welcome/index'

  root 'welcome#index'
  resources :log
  resources :updates

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
