class LogController < ApplicationController
  skip_before_action  :verify_authenticity_token
  def index
    @events = Event.all
    # render json: @events
  end

  def create
    @event = Event.new(event_params)
    @event.save
  # redirect_to @event
  end

  def list
    @events = Event.all
    render json: @events
  end

  def find
    @events = Event.where(title: params[:title])
    render json: @events
  end

  def show

  end

private
  def event_params
    params.permit(:event_name, :user, :uuid)
  end

end
