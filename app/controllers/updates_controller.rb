class UpdatesController < ApplicationController
  skip_before_action  :verify_authenticity_token

  def index
    @firmwares = Firmware.all
  end

  def check
    @lastest_firmware =  Firmware.all.order('id DESC').first
    render json: @lastest_firmware
  end

  def create
    @firmware =  Firmware.new(event_params)
    @firmware.save
  end


  private
  def event_params
    params.permit(:version, :md5, :url)
  end

end
