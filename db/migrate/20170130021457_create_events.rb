class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :event_name
      t.string :user
      t.string :uuid

      t.timestamps
    end
  end

end
